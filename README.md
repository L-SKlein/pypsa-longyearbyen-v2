# PyPSA-Longyearbyen V2

This model and its descriptions are based on the model of Koen van Greevenbroek, developed during the course AGF-353 "Sustainable Arctic Energy Exploration and Development" in June 2021. It is licensed under GNU General Public License v3.0.

Source of the basis model: https://gitlab.com/koenvg/pypsa-longyearbyen

(Further information on the overall project can be found in https://www.unis.no/wp-content/uploads/2021/07/koen_lars_thermal_storage_updated.pdf)

## Setup

Create a conda environment for the PyPSA-Longyearbyen by running
```shell
conda env create -f envs/environment.yml
```

By default the model is configured to use the Gurobi solver.
(See https://www.gurobi.com/academia/academic-program-and-licenses/ for academic licences.)

However, it is easily configurable to use an open source linear program solver such as GLPK or CBC by changing the `solver_name` argument for the `pypsa.Network.lopf` function.
To install these two solvers, simply run `conda install -c conda-forge coincbc` or `conda install -c conda-forge glpk` in the `pypsa-longyearbyen` conda environment.

## Running the model

Activate the conda environment (`conda activate pypsa-longyearbyen`) and open a python shell (`python`) or Jupyter notebook.
Load the module (`import model`) and build and solve the model by calling `n = model.create_model()`.

Running the model over a 1 year period using the Gurobi solver takes about half a minute.
Running the model over the full 9 year period using the Gurobi solver can take 5-10 minutes and 2-3 GB of memory.


## Licenses

All code in this repository is licenced under the [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later.
The licencing of the data used to run the model is documented in `data/README.md`.
All other supplementary data in this repository (solved models, figures, etc.) are licenced under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC BY 4.0) licence.


