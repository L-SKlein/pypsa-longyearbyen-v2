import matplotlib.pyplot as plt
#matplotlib inline

import pypsa
import yaml
import pandas as pd

import model

n = model.create_model()

from pdb import *
#set_trace()

print ("Total installed wind capacity : "+ str(n.generators["p_nom_opt"].iloc[:15].sum()))
print ("Total installed PV capacity : "+ str( n.generators["p_nom_opt"]["PV"]))
print ("Battery storage: "+ str(n.storage_units["p_nom_opt"] * 6))
print (n.links["p_nom_opt"])

fix, axs = plt.subplots(3, figsize=(8, 6), gridspec_kw={'height_ratios': [2, 1, 1]})
n.loads_t["p_set"].loc["2015", "electricity demand"].plot(lw=0.5, ax=axs[0])
n.loads_t["p_set"].loc["2015", "heat demand"].plot(lw=0.5, ax=axs[0])
n.generators_t["p_max_pu"].loc["2015", "LH TUGE50"].plot(color='green', lw=0.5, ax=axs[1])
n.generators_t["p_max_pu"].loc["2015", "PV"].plot(color='red', lw=0.5, ax=axs[2])
axs[0].set_ylabel("Load [MW]")
axs[0].set_xlabel("")
axs[0].set_ylim(ymin=0)
axs[0].legend()
axs[1].set_ylabel("Wind cap. fac.")
axs[1].set_xlabel("")
axs[1].set_ylim(ymax=1.05)
axs[2].set_ylabel("PV cap. fac.")
axs[2].set_xlabel("")
axs[2].set_ylim(ymax=1.05)
plt.tight_layout()
plt.savefig("figures/demand.pdf")

period = "2015"

fig, axs = plt.subplots(2, 2, figsize=(9, 5))

n.stores_t["e"].loc[period, "hydrogen storage"].plot(ax=axs[1][0], lw=0.5)
axs[1][0].set_title("Ammonia storage")

n.stores_t["e"].loc[period, "hot water storage"].plot(ax=axs[0][0], lw=0.5)
axs[0][0].set_title("Hot water storage")

n.stores_t["e"].loc[period, "molten salt storage"].plot(ax=axs[0][1], lw=0.5)
axs[0][1].set_title("Molten salt storage")

n.storage_units_t["state_of_charge"].loc[period, "batteries"].plot(ax=axs[1][1], lw=0.5)
axs[1][1].set_title("Battery storage")

for ax in axs.flatten():
    ax.set_ylabel("State of charge [MWh]")
    ax.set_xlabel("")
    
plt.tight_layout()
plt.savefig("figures/states_of_charge.pdf")

